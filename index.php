<?php
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('Asia/Shanghai');
set_time_limit(0);
define('ENVIRONMENT', 'production');
//define('ENVIRONMENT', 'development');
//define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');
if (defined('ENVIRONMENT')) {
	switch (ENVIRONMENT) {
		case 'development':
			error_reporting(E_ALL);
		break;
		case 'testing':
		case 'production':
			error_reporting(0);
		break;
		default:
			exit('The application environment is not set correctly.');
	}
}

$system_path = 'system';
$application_folder = 'application';
// *************** PHP7 START ***************
if(!function_exists('mysql_pconnect')){
    function mysql_pconnect($dbhost, $dbuser, $dbpass){
        global $dbport;
        global $dbname;
        global $mysqli;
        $mysqli = mysqli_connect("$dbhost:$dbport", $dbuser, $dbpass, $dbname);
        return $mysqli;
    }
    function mysql_select_db($dbname){
        global $mysqli;
        return mysqli_select_db($mysqli,$dbname);
    }
    function mysql_fetch_array($result){
        return mysqli_fetch_array($result);
    }
    function mysql_fetch_assoc($result){
        return mysqli_fetch_assoc($result);
    }
    function mysql_fetch_row($result){
        return mysqli_fetch_row($result);
    }
    function mysql_query($cxn){
        global $mysqli;
        return mysqli_query($mysqli,$cxn);
    }
    function mysql_escape_string($data){
        global $mysqli;
        return mysqli_real_escape_string($mysqli, $data);
    }
    function mysql_real_escape_string($data){
        return mysql_real_escape_string($data);
    }
    function mysql_close(){
        global $mysqli;
        return mysqli_close($mysqli);
    }
}
if (defined('STDIN')) {
	chdir(dirname(__FILE__));
}

if (realpath($system_path) !== FALSE) {
	$system_path = realpath($system_path).'/';
}

	
$system_path = rtrim($system_path, '/').'/';

if ( ! is_dir($system_path)) {
	exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
}

define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

define('EXT', '.php');

define('BASEPATH', str_replace("\\", "/", $system_path));

define('FCPATH', str_replace(SELF, '', __FILE__));

define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));

if (is_dir($application_folder)) {
	define('APPPATH', $application_folder.'/');
} else {
	if ( ! is_dir(BASEPATH.$application_folder.'/')) {
		exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
	}
	define('APPPATH', BASEPATH.$application_folder.'/');
}

define('JXC_VERSION', 'jxc 2.0.1');
//echo BASEPATH.'-----';
//  // /Users/kunzhang/www/ERP-master/system/
require_once BASEPATH.'core/CodeIgniter.php';
/* End of file index.php */
/* Location: ./index.php */